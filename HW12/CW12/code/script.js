let [...allInputs] = document.querySelectorAll("input");

localStorage.user = JSON.stringify([]);

class User {
    constructor(name, lastname, age, number, index) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.number = number;
        this.index = index;
        this.status = true;
    }
}

const createUserList = () => {
    //Отримуємо масив інформації із памяті
    let currentUsers = JSON.parse(localStorage.user);

    currentUsers.forEach(function (e) {
        let tr = document.createElement("tr");
        let tdName = document.createElement("td");
        let tdLastName = document.createElement("td");
        let tdAge = document.createElement("td");
        let tdNumber = document.createElement("td");
        let tdIndex = document.createElement("td");
        let tdStatus = document.createElement("td");

        let inputCheckbox = document.createElement("input");
        inputCheckbox.checked = e.status;
        inputCheckbox.type = "checkbox";

        tdName.innerText = e.name;
        tdLastName.innerText = e.lastname;
        tdAge.innerText = e.age;
        tdNumber.innerText = e.number;
        tdIndex.innerText = e.index;

        tdStatus.append(inputCheckbox);
        document.querySelector("tbody").append(tr);
        tr.append(tdName, tdLastName, tdAge, tdNumber, tdIndex, tdStatus)

    })
}


let inputRez = allInputs.map(function (e) {
    return e;
}).filter((e) => { return e.type != "button" });

const validate = (target) => {
    switch (target.id) {
        case "name": return /^[A-z]{2,}$/i.test(target.value);
        case "lastname": return /^[А-я]{2,}$/i.test(target.value);
        case "age": return /^\d{1,2}$/.test(target.value);
        case "phonenumber": return /^\+380\d{9}$/.test(target.value);
        case "index": return /^\d{5}$/.test(target.value);
        default: throw new Error("Невірний виклик регулярного виразу")
    }
};

inputRez.forEach((e) => {
    e.addEventListener("change", (event) => {
        validate(event.target);
        if (validate(event.target) == false) {
            let [...span1] = document.querySelectorAll("span");
            span1.forEach(function (element) {
                element.remove()
            })
            e.style.border = "2px solid red";
            let span = document.createElement("span");
            span.innerText = "Введіть правильно дані";
            span.style.color = "red";
            span.style.fontSize = "14px";
            e.after(span);

        }
        else  {

            e.style.border = "";

            let [...span] = document.querySelectorAll("span");
            span.forEach(function (element) {
                element.remove()
            });
        }
    })
})

let saveButton = document.querySelector("[type = button]");

saveButton.addEventListener("click", () => {
    //Функція валідації після натиску кнопки
    let validateRez = inputRez.map(function (element) {
        return validate(element);
    });
    //Перевіряємо чи немає помилок в інпутах
    if (!validateRez.includes(false)) {
        //Якщо немає помилок,записуємо в localStorage
        let a = JSON.parse(localStorage.user);
        a.push(new User(...inputRez.map(
            (e) => { return e.value }
        )))
        localStorage.user = JSON.stringify(a);
        createUserList();
        inputRez.forEach((e) => e.value = "")
    }


})
