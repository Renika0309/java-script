const ingridients = document.querySelectorAll(".draggable"),
pizza = document.querySelector(".table"),
sous = document.querySelector(".sauces"),
toping = document.querySelector(".topings"),
radio = document.querySelectorAll(".radioIn");

radio.forEach(radio => {
radio.addEventListener(
    "click",
    function (e) {
        let price = document.getElementById("price");
        if (e.target.id == "small") {
            price.innerText = "75грн";
        }
        else if (e.target.id == "mid") {
            price.innerText = "120грн";
        }
        else if (e.target.id == "big") {
            price.innerText = "150грн";
        }
    }
)
})

ingridients.forEach(ingridient => {
ingridient.addEventListener(
    "dragstart",
    function (ev) {
        ev.dataTransfer.setData("text", ev.target.id);
        ev.dataTransfer.effectAllowed = "copy";
    })
})

pizza.addEventListener("dragover",
function (e) {
    if (e.preventDefault) e.preventDefault();
    return false;
})

pizza.addEventListener("drop",
function (ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    var copyimg = document.createElement("img");
    var original = document.getElementById(data);
    copyimg.src = original.src;
    this.appendChild(copyimg);
    let name = document.createElement("p");
    name.style.fontSize = "14px";
    let name2 = document.createElement("p");
    name2.style.fontSize = "14px";
    sous.append(name);
    toping.append(name2);


    switch (data) {
        case "sauceClassic":
            name.innerText = "Кетчуп";
            break;
        case "sauceBBQ":
            name.innerText = "BBQ";
            break;
        case "sauceRikotta":
            name.innerText = "Рікотта";
            break;
        case "moc1":
            name2.innerText = "Сир звичайний";
            break;
        case "moc2":
            name2.innerText = "Сир фета";
            break;
        case "moc3":
            name2.innerText = "Моцарелла";
            break;
        case "telya":
            name2.innerText = "Телятина";
            break;
        case "vetch1":
            name2.innerText = "Помiдори";
            break;
        case "vetch2":
            name2.innerText = "Гриби";
            break;

        default: return;
    }
}
)

const banner = document.getElementById("banner");
banner.addEventListener("mouseover", () => {
banner.style.bottom = Math.floor(Math.random() * 80) + "%";
banner.style.right = Math.floor(Math.random() * 80) + "%";
}
)
banner.addEventListener("mouseout", (e) => {
banner.style.bottom = Math.floor(Math.random() * 80) + "%";
banner.style.right = Math.floor(Math.random() * 80) + "%";
}
)

const [...allInputs] = document.querySelectorAll("input");
localStorage.user = JSON.stringify([]);

class User {
constructor(name, number, email) {
    this.name = name;
    this.number = number;
    this.email = email;
    this.status = true;
}
}
let inputRez = allInputs.map(function (e) {
return e;
}).filter((e) => { return e.type != "button" && e.type != "reset" && e.type != "radio" });

const validate = (target) => {
switch (target.id) {
    case "name": return /^[А-я]{2,}$/i.test(target.value);
    case "phone": return /^\+380\d{9}$/.test(target.value);
    case "email": return /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/.test(target.value);
    default: throw new Error("Невірний виклик регулярного виразу")
}
};
inputRez.forEach((e) => {
e.addEventListener("change", (event) => {
    validate(event.target);
    if (validate(event.target) == false) {
        let [...span3] = document.querySelectorAll("span");
        span3.forEach(function (element) {
            element.remove()
        })
        e.style.border = "2px solid red";
        let span1 = document.createElement("span");
        let span2 = document.createElement("span");
        span1.innerText = "Введіть правильно дані";
        span1.style.color = "red";
        span1.style.fontSize = "14px";

        e.after(span2, span1)

    }
    else {

        e.style.border = "";

        let [...span] = document.querySelectorAll("span");
        span.forEach(function (element) {
            element.remove()
        });
    }
})
})
let saveButton = document.querySelector("[type = button]");

saveButton.addEventListener("click", () => {
//Функція валідації після натиску кнопки
let validateRez = inputRez.map(function (element) {
    return validate(element);
});
//Перевіряємо чи немає помилок в інпутах
if (!validateRez.includes(false)) {
    //Якщо немає помилок,записуємо в localStorage
    let a = JSON.parse(localStorage.user);
    a.push(new User(...inputRez.map(
        (e) => { return e.value }
    )))
    localStorage.user = JSON.stringify(a);
    inputRez.forEach((e) => e.value = "");
    location.href = 'thank-you.html';

}

})
let resetButton = document.querySelector("[type = reset]");
resetButton.addEventListener("click", () => {
inputRez.forEach(function (element) {
    element.value = "";
    element.style.border = "";

    let [...span] = document.querySelectorAll("span");
    span.forEach(function (element) {
        element.remove()
    })
    
})
})