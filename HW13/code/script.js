let url = "https://swapi.dev/api/people";

const promise = fetch(url);

let data = promise.then((rez) => {
    return rez.json();
});

let people = data.then((peoples) => {
    let container = document.createElement("div");
    container.className = "container";

    peoples.results.forEach((people) => {
        const { name, birth_year, gender, homeworld } = people;

        let card = document.createElement("div");
        let cardName = document.createElement("p");
        let cardBirth = document.createElement("p");
        let cardGender = document.createElement("p");
        let cardHomeWorld = document.createElement("p");

        card.className = "card";
        cardName.className = "card_title";
        cardGender.className = "card_title";
        cardHomeWorld.className = "card_title";
        cardBirth.className = "card_title";

        cardName.innerText = `Name: ${name}`;
        cardBirth.innerText = `Birth year: ${birth_year}`;
        cardGender.innerText = `Gender: ${gender}`;
        
        let btn = document.createElement("input");
        btn.type = "button";
        btn.value = "Read more";
        btn.className = "btn";

        let urlHomeWorld = fetch(homeworld)
            .then((rez) => {
                return rez.json();
            })
            .then((planets) => {
                cardHomeWorld.innerText = `Planet: ${planets.name}`;
                card.append(cardName, cardBirth, cardGender, cardHomeWorld, btn);
            });
        

        container.append(card);
        document.querySelector("body").append(container);

        btn.addEventListener("click", function(){
            switch(name){
                case"Luke Skywalker": location.href = "https://en.wikipedia.org/wiki/Luke_Skywalker";
                break;
                case "C-3PO": location.href = "https://en.wikipedia.org/wiki/C-3PO";
                break;
                case "R2-D2": location.href = "https://en.wikipedia.org/wiki/R2-D2";
                break;
               case "Darth Vader": location.href = "https://en.wikipedia.org/wiki/Darth_Vader";
               break;
                case "Leia Organa": location.href = "https://en.wikipedia.org/wiki/Princess_Leia";
                break;
                case "Owen Lars": location.href = "https://starwars.fandom.com/wiki/Owen_Lars";
                break;
                case "Beru Whitesun lars": location.href = "https://starwars.fandom.com/wiki/Beru_Whitesun_Lars";
                break;
                case "R5-D4": location.href = "https://starwars.fandom.com/wiki/R5-D4";
                break;
                case "Biggs Darklighter": location.href = "https://starwars.fandom.com/wiki/Biggs_Darklighter";
                break;
                case "Obi-Wan Kenobi": location.href = "https://starwars.fandom.com/wiki/Obi-Wan_Kenobi";
            }
         
        })
    })
})
