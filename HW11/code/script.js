  
               //Завдання 1
               class User {
                constructor( name, surname){
                    this.name = name;
                    this.surname = surname;
                }
                show () {console.log(this.name + " " + this.surname)}
               }
               const user1 = new User("Renata", "Slavik");
               user1.show();
               /*
               //Завдання 2
               const ul = document.createElement("ul");
               ul.style.width = "30%";
               ul.textContent = "Фрукти";
               document.body.prepend(ul);
               const li = document.createElement("li");
               li.textContent = "Банан";
               const li1 = document.createElement("li");
               li1.textContent = "Яблуко";
               const li2 = document.createElement("li");
               li2.textContent = "Ягоди";
               const li3 = document.createElement("li");
               li3.textContent = "Вишня";
               ul.append(li);
               ul.append(li1);
               ul.append(li2);
               ul.append(li3);
               ul.firstElementChild.style.background = "blue";
               li2.style.background = "red";
               //Завдання 3
               const div = document.querySelector(".mouse");
               div.onmouseover = function(e){
                //console.log(`X:${e.clientX} / Y:${e.clientY}`)
                div.textContent = `X:${e.clientX} / Y:${e.clientY}`;
               } */
        //Завдання 4
        const btn = document.querySelector(".btn");
        btn.addEventListener("click", (e) => {
            if (!e.target.classList.contains("button")) return;
            const span = document.createElement("span");
            span.style.position = "relative";
            span.textContent = ` Кнопка під номером ${e.target.value}`;
            btn.after(span);
            e.target.style.backgroundColor = "green";
        }, false);

        //Завдання 5
        const div = document.querySelector(".mouse");
        div.addEventListener("mouseover", (e) => {
            div.classList.add("position")
        }, false);
        div.onmouseout = function () {
            div.classList.remove("position")
        }

        //Завдання 6
        const input = document.querySelector(".color");
        input.addEventListener("input", (e) => {
            document.body.style.backgroundColor = input.value;
        }, false);
        //Завдання 7
        const login = document.getElementById("login");
        login.addEventListener("input", (e)=>{
         console.log(login.value)
        })
        //Завдання 8
        const date = document.getElementById("date");
        date.addEventListener("change", (e) => {
            const span = document.createElement("span");
            span.style.position = "relative";
            span.textContent = date.value;
            date.after(span);
           
        }, false);
          
        